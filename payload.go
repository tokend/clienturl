package clienturl

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

type Payload struct {
	// Type specific redirect type which meta corresponds to
	Type string `json:"type"`
	// Meta holds additional information need for redirect processing
	Meta interface{} `json:"meta,omitempty"`
}

func NewPayload(redirectType string, meta interface{}) Payload {
	return Payload{
		Type: redirectType,
		Meta: meta,
	}
}

// Encode with base64url without padding
func (p Payload) Encode() (string, error) {
	bytes, err := json.Marshal(p)
	if err != nil {
		return "", err
	}
	encoded := base64.URLEncoding.WithPadding(base64.NoPadding).EncodeToString(bytes)
	return encoded, nil
}

// MustEncode Encode analogue that panics instead of returning error
func (p Payload) MustEncode() string {
	encoded, err := p.Encode()
	if err != nil {
		panic(err)
	}
	return encoded
}

// BuildUrl builds final redirect link for the given URL
func (p Payload) BuildUrl(clientUrl string) (string, error) {
	encoded, err := p.Encode()
	if err != nil {
		return "", fmt.Errorf("failed to encode payload: %v", err)
	}
	return fmt.Sprintf("%s/r/%s", clientUrl, encoded), nil
}

// MustBuildUrl BuildUrl analogue that panics instead of returning error
func (p Payload) MustBuildUrl(clientUrl string) string {
	url, err := p.BuildUrl(clientUrl)
	if err != nil {
		panic(err)
	}
	return url
}