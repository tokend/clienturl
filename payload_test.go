package clienturl

import (
	"encoding/base64"
	"testing"
)

func TestPayload(t *testing.T) {
	cases := []struct {
		name     string
		payload  Payload
		expected string
	}{
		{
			"simple",
			Payload{
				Type:   "1",
				Meta: map[string]interface{}{
					"foo": "bar",
				},
			},
			`{"type":"1","meta":{"foo":"bar"}}`,
		},
		{
			"empty meta",
			Payload{
				Type:   "1",
				Meta:   0,
			},
			`{"type":"1","meta":0}`,
		},
		{
			"struct meta",
			Payload{
				Type: "1",
				Meta: struct {
					Foo string `json:"foo"`
				}{
					Foo: "bar",
				},
			},
			`{"type":"1","meta":{"foo":"bar"}}`,
		},
		{
			"nil meta",
			Payload{
				Type:   "1",
			},
			`{"type":"1"}`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			encoded, err := tc.payload.Encode()
			if err != nil {
				t.Fatal(err)
			}

			json, err := base64.URLEncoding.WithPadding(base64.NoPadding).DecodeString(encoded)
			if err != nil {
				t.Fatal(err)
			}

			if string(json) != tc.expected {
				t.Fatalf("expected %s got %s", tc.expected, json)
			}
		})
	}
}
